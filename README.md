# multielement

"Multielement" is a JQuery plugin, which adds buttons next to a form line, which allows duplication.
The line to be duplicated must be a table row.