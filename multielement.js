
(function ($) {
    
    
    
    var titlebtnup="Add row above";
    var titlebtndown="Add row below";
    var titlebtndelete="Delete row";

$.fn.multielement = function(classname) {

//html code for buttons
buttons_html = '<td class="multielement-btn-up">'+
        '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAgCAMAAABjCgsuAAABR1BMVEXf39/////+/v709PTx8fHw8PDu7u7t7e3s7Ozr6+vq6urp6eno6Ojn5+fm5ubl5eXk5OTj4+Pi4uLh4eHg4ODe3t7d3d3c3Nza2trX19fW1tbV1dXT09PS0tLR0dHQ0NDPz8/Ozs7Nzc3MzMzLy8vKysrJycnIyMjHx8fGxsbFxcXExMTDw8PCwsLAwMC/v7++vr68vLy5ubm4uLi3t7e2tra1tbW0tLSzs7OysrKxsbGwsLCvr6+urq6tra2rq6uqqqqpqamoqKinp6empqalpaWjo6OioqKhoaGgoKCfn5+enp6dnZ2cnJybm5uZmZmXl5eWlpaVlZWUlJSSkpKRkZGOjo6Li4uKioqJiYmHh4eGhoaFhYWEhISDg4OBgYGAgIB/f39+fn56enp5eXl3d3d2dnZzc3NycnJxcXFubm5ra2v///9k1yUtAAAAAXRSTlMAQObYZgAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfiBhIGATWpLfNNAAAB9UlEQVQ4y63Ua0/iQBQGYGaQFlqHQpkABTqbahELimuCUZtCReuFgqSsiOtlvRDEXfv/v++UCmZdYyzx/Xye5Jy5nFDonTAcx/Mcx4Q+m0CAYRDCkQgAkQhGiGG+GkSjyaQkcQCMRhDGJCmZjEY/LE+lCgWE4NXVaOS6AKFCAeMPSFBAywkRBAjPfOACIAiEUPI1gPZDSCIBYbd7dnExHD4/v5J3uwoIvHkn5Z1Ot+ucn9/cjMeuOyX/TT4pJ7TctjudVqvd6w0Gd3dP7pSQtyQo8MsTtB3bbtG06Rh0jvunKXl7WAEBy4qy7M/7Chyn3//58PBCvMllOcWy84F4XKLPB1qWdXDQbO7vHx4enZy0KaKT397OThchKR6fB7AsxjEIt7ZqfnZ2Gg2z2ZxI2+71vEv/45MYxrQpjqPfa22tXNa08uamB3Z3DcPQ9Xq9YZqWdXra7/96fPQeClhY4PnggOcBUBWa5eUZoNH91Ot7e8fHPy4vh8Px+DcAi4vzgW+EKMrq6vdabXv7X6DrhmGaR44zGFxf3wPw0lIgwDDhMPhkwuHJnqK7KJPJ5wlZKhZLJU2rVNar1Y2NanW9UtG0UqlYXCIkn89kpjsqMKC3zfOCIIo4nc5mczlZVlR1ZUVVFVnO5bLZdBqLoiDwPDv7D0HAX2c8xdyhyWqlAAAAAElFTkSuQmCC" class="btnup" alt="" title="'+titlebtnup+'" /></td>' +
        '<td class="multielement-btn-down"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAgCAMAAABjCgsuAAABR1BMVEXf39/////+/v709PTx8fHw8PDu7u7t7e3s7Ozr6+vq6urp6eno6Ojn5+fm5ubl5eXk5OTj4+Pi4uLh4eHg4ODe3t7d3d3c3Nza2trX19fW1tbV1dXT09PS0tLR0dHQ0NDPz8/Ozs7Nzc3MzMzLy8vKysrJycnIyMjHx8fGxsbFxcXExMTDw8PCwsLAwMC/v7++vr68vLy5ubm4uLi3t7e2tra1tbW0tLSzs7OysrKxsbGwsLCvr6+urq6tra2rq6uqqqqpqamoqKinp6empqalpaWjo6OioqKhoaGgoKCfn5+enp6dnZ2cnJybm5uZmZmXl5eWlpaVlZWUlJSSkpKRkZGOjo6Li4uKioqJiYmHh4eGhoaFhYWEhISDg4OBgYGAgIB/f39+fn56enp5eXl3d3d2dnZzc3NycnJxcXFubm5ra2v///9k1yUtAAAAAXRSTlMAQObYZgAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfiBhIFOzGJr1d0AAAB+klEQVQ4y63Ua1PaQBQG4OwiJGQNMWEHCJDtRIMYUKwzONpOJFbx0hgdKAjeLwzGlvz/792ES+3dpH0/n2fmnN2zyzA0LEKiKMs4k8nl8nlV1XR9eVnXNVXN53O5TAbLsigixDLThAQcJ2WzhQIhi6VSuWwY1eparba+XqutVauGUS6XSouEFArZrMRxUUAiEYuBVyYWSyQYhAB4uL7u9VqtQ8syzffTvPOzvb21Va+/XVnRNELeAIBQeDA/D8Bn1x0MLi8/HR3t7u7sfA/q9Y2NytKSRqNHAwjNzQHPG42enm673dPTZtOyGr4yTXMGDKNSWV0F8TjPhwcsi3ESUOJ5XwaDfr/dtu2Dvb0PNFaj4YMgm5sQJjFm/dtOpRRBGBPPde/uer322dnJyfHx4UEg9/ebzSaEgqCkUsFNhwYsm1ZVUZyQ5+fHx4tut9Wi6CONbTuOA+HCgqrKLBsN0PXDmBBKvAl56Pc7nReAlhOSTnOz9xAa0A0PCJiS+3s6eZsCx7Ft2k9Q/qI+PJiQ2eSue3Nzfk7nHs/7c/mEkCkZjfxLp2PAcfu/KI8Avh2WT4bDq6sOhKL4w/H8GwgmLxbpYo0BXZ9iMf27fiKB4I+SFCUJ4XAIAK8okvTHcib4pwQBx+OAPi8sCPQfYv43CBDPI8Tzryv+G/gKyVnF3F2bLsYAAAAASUVORK5CYII=" class="btndown" alt="" title="'+titlebtndown+'" /></td>' +
        '<td class="multielement-btn-delete"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4gYSBToAwWpmDwAAC7VJREFUWMONl2uMXdV5ht9129dzZuacYc7YZzxjjM11xmDXFGgICQbTEKUCoUQKrZCoIFBbOEGiVfKjxUnVigYJhaYKtsEmWFBEG7lKqaqmOIEA5tKAsY1FcfFgz9X2XM797Ptel/7wjNuCK2X92lpbWt+rd33v8+1NHn7kEfz4Rz8CAGz79rf7r7v22js551eBUq6k/K+jH374T3/7xBOL+C3X1ocewu6nngIA/PVjj62/+OKLHyGEDCRJ8u/dbvftiYmJj3785JM5APzzyy8TAgC7n37a6uvrK5dKpTHLtm8lhKwDwLTWp6I4fmX2zJljp8+cqf3Vo4+q30bEUzt32mvXrh3hQtxIgLtBSIkx9j4h5FAcx4drCwuTnPPg7rvv1hwABgcHh0ql0h09PT1fKpVKY5ZlVQghJEmSsYXFxUsopa8ZrX8GYOH/rcoYoM7pW7Vq1apKpfKg6zi3ep43wjl3QcglcRz/zszMzFuE0re0MQcBNDkA2wBVzvkXhRA3u65bdl0XxhgQQnoLhUJZG0Nknp+8/8EH33n2mWc6AMznBCiFG2+6yb558+Yh3/dvtR1ni+d5GwqFAoQQUFp7Sqlezrll23YeRdFxAE369W9+c1BKuZJxPsw4LxNCYIyBlBKEEJRKpf6L+vs3uK779Ruuv/72b23d2vO/6971jW+cf75ly5ahsdHRrZTzba7jrPN8H9oY5FKCEgLGmOBCXO563nptzAoANi/4vl+r1wuM894oigBj0NPbazhjoJQSxhiUbQ/19fbeuHr1alQqle61GzceWpifr+/YsUP9fP9+7PjBD+wrr7hiqFQqbRGWdVvB9zdwIcAYg9YaWZahk6ZoNBpYWFgotFqtSrPV8ldUqxanlJoTJ07oqakp3V8u49JLL8XIyAgZrFTgOA6UUqCU8oGBgUt833cbjcYAZ2wdZewfAcwDwOjo6PBQtfonnufd5hcKa23LAgGQZRlsy0KQpjh58iQmp6ZwenYW7U5H5Xlurrv+enBCSNJoNlMYk0RhCEopjDGwLAvlUsks9QKxLMsyxqz2PK9HG6MreX78oe9852jB93sKvn+T47pbXNe9xnNdCCGglUKe5yZLU8zPz5OJyUlMTk6iXq8nURS1QUiycuXKnGut25YQbQDdXEo9PTNDcylhOw4AoFgogHOOPM+htUahp6fEhbgqDMMtGzdsWG9Z1hrO+VWu44wsOwYAQggkaUrOnD1rTk1MYGpqCvVGw2ita7ZtzxljOk/v3JlwpVRo2/YcIeQ9pZTX6XTWUcZKk5OT0FqTFYODKBaL4IwZQinhlEIIUfULhS+sWLEioYSsdl23whgrUkqhtIZMU4RhiHq9jlOnTpHp6Wk0W608ieMZy7IOCSHeN8bUAICtrFZVb29vaIyZAtCklFa11pUgCHgQBNBag1IK13HIsrVaKSY4LxWLxWpPsdjveZ5LKaXGGMIpRRBFmJqeNuPj42RichK1ej2XeX6SEPImgH8ghLymta59cOiQ4sPDw3hm164ugI8e2LpVM8aGlFJot9tXSin7KKUwWoNzjnJfn1kCAONC9AIAoxQAYIxBmqYmjmMs1GpkYmqKnJ6dRbPZRJqms4yxtxljv5J5/u6e3bubAHDvffeBHf7gg/M5vmbDhsgYM00p7XDOR4wxK6MoImmWgQsByhiW47nkBJRSy9BCkiSYm5vD9PQ0mZmZQbvdhtZ6gVD6GwK8lGXZ68aYdrvbNa1GAx8eOQK+XPyP778fz+3dGwL4eOtDD4ExNqyUyrvd7hohRH8QBDyJY+o6DhhjUEpBa41lcFFKIaUkURwjDEMVhmEUx/FpIcQxztgbWutD+559tvlZgJ4XsO/ZZ89vaq0nCCF7syw7JqW8kzF2nW1ZVcaYbYyBkhJK68+Q+Fz327aNYqHQFUIcSdP0XaXUa67jfJTlee1CI4QCwJ9973vnN+574AE8s2tXvOsnPzlZLBY/XrlyZTQwMMCWmQ5joM3/jIJlB5YFeK6L3r4+DAwMyIGBgYZlWR/t3rlz/qd79qg/uPPOCwt44vHHz4v46Z49y+/sVatWVS+/7LLLVo+MDJf6+mzXcc6DihACSinIuYm+TEz4vo9SX58/WKmsX7NmzdXr1q4dWD5w/dVXf04A+ezGxk2b+M233DLo+/4VQ9Xqlv5y+R7P81YVfB+2bRsCELV0DcYYsHNNuWwHKCGI4xi1el2HUXRUKbUvjuPXF2u1qb989NHO56b4n+/YgYNvvAEAcH2ffuHGG4eq1eoX+/v7v2Xb9tcY5wOEEJbnOXIpoY0haZqiVq+j1W4jS1NkeX4OQFIiTBITpymMMZRS2gNglBCyUub56V8dODAPQF3QgTvuukuMjo6utm37hmKxuMX3vDst2+5zHAdCCMMYA+ccjm0jiiJy5uxZxHG83HQo9/fDsW2TSwmlFFlu1jTLkOf5TBzHz3c6nVeDIPjPv/nhDxfTMDQAwADg97/6VTE0NLSuXC7fUj5n+WbHcXotyyJCCFiWBcdxiLU05YIgIHNzc6jX6+h2uzBao1wqoaenBxbnRAgBSggIpWCcgzJWoJSOWpZV1VrPA1gcP3EiAQDy2OOPO1LKEQC3+L5/W6FQ+Ipt276wLNiWZSwhIIQgnHNIKREEARr1upqenW21Wq3MGCPK5bK9ds0av1Kp0OJSWqSUyJUySimkWUaSJEGWZe04jv8+DMNXGGNHy+XyPPvy5s2/6/v+tcVi8Y88z7tpqTixLAu2bcN3XbJ0oGm222RmdhYLCwtRFEX/kef5MQDzxhgZRVF/mmXc8zx4nmcs2yaCc2LORZYwxkAZsznn62zHGeScL0ZRlPM0TS91XfdyIcSYEKIghIBtWdq2beI4DmzbNlmWkXanQ2q1GhqNhux0u+NKyoNKqU8JIX1pmo7mUhYMcKnneZQyRvrLZdiWZQgAxpjJ89zQcxkuA9iUxPHBVqvV5mmaVvM8L1NKDeccSwKo57pwbNtQShHHMU6fOYPFxUWZJMlxGPMmIeSXcRx/rLW2enp6ThGgFEWROzk5OZwkCXEcB57vw2UMQgiSJAnBErAyKVUURb2NRqPCpZRekiR2GIaaUgrPdSEsC4wxk2UZiZYy3Wq1up1OZxzAWwBep5Qef2HfvhBA+MDWrUeEEMNZlpkkSTZxxi6eL5c9QggpeB445+CcGwOQMIrQ6XRMEAQiSRKXSynzMAylUooopVDq64MlhMmzzLTabXLm7Fk0m80wCIJfSylfB/AW5/wEISQEgNtuvx0AmkmS/Aul9FMAp7pB8KUT4+O/V2803JHhYZTLZW3bNjIpydzcHBYXF0mSJEpKmXOt9VSaZY6UMrAtq5IkCcIwJHEck3qjYWq1WtRqtT7M8/y1JEl+PTMzc/zVAwcUAPzF979PLrroIvPSSy/p37zzTv1rd9zxfrVaVXEcqyzLeqWUY5ZlWbmU1Pd9tNttNJtNtNvtjlJqUSl1ll2zcWPMGGMArhRCDAnLokmSYLFWQ7vdDjqdzsEoin6htP5l0O1+8q8vv6wA4E+/+10wxmDbNp7buxcAMP7JJ3J0bGxBCNEBICiltpSyEgQB73a7aLVa6HQ6cZamx7TW/xaG4TFWWbEi6OnpASGkVwhBOeckyzIVBEE9DMMjcRwf6HQ6B5uNxvGf79+fA8C27dvxd08+iYNvvokDr7zyf9h+7OhRdfWGDS0AihCitNYiz3MryzKZpulckiRHpJRvSynffvHFF6fYp+PjanRsLFZKTTmOM0MpVXmez6RpeiDPsl/kUh6sNxqfvv3uu2kcBACAQ++9h23bt+PQe+9d8Ddx9Zo1yrKsedu2TxNCzgKYBVBTSh3Lsmx/t9t9NU3T2cPvv5+xe+69Fy/s25cePXy4tnHTJsUYG5RSxlmaHk7i+BjReuKF55/P4iDAg9u24YNDh86LuNDa/vDDeP6553D08OH8uhtuaCml9NI3o9BanwnD8I09u3efPHb0aP6H99yD/wa/oj3eQ0TPkAAAAABJRU5ErkJggg==" class="btnremove" alt="" title="'+titlebtndelete+'" /></td>';
        //find the row of the table to be made multiple and add the buttons at the end. 
        $("." + classname).find("td:last").after(buttons_html);
        deleteBtnShowHide(); //This function hides the delete button if there is only one line or shows in the case of multiple lines.

        //only the first line exists during initialization. The html code is saved in a variable to be used later
        var structure = $("." + classname).parent().html(); //the row' s code also includes tags <tr class="" ...>...</tr>

        events_init(); //event capture click on the buttons initialization

        //events functions 

                function events_init(){

                $(document).on("click", "." + classname + " .multielement-btn-up img", function(){       //click event on button UP

                $(this).closest("tr").before(structure);
                        deleteBtnShowHide();
                });
                        $(document).on("click", "." + classname + " .multielement-btn-down img", function(){ //click event on button DOWN

                $(this).closest("tr").after(structure);
                        deleteBtnShowHide()
                });
                        $(document).on("click", "." + classname + " .multielement-btn-delete img", function(){ //click event on button DELETE

                $(this).closest("tr").remove();
                        deleteBtnShowHide();
                });
                }


        function deleteBtnShowHide(){ //This function hides the delete button if there is only one line or shows in the case of multiple lines.
        if ($("." + classname).length == 1){
        $("." + classname + " .multielement-btn-delete img").hide();
        } else{
        $("." + classname + " .multielement-btn-delete img").show();
        }
        }//close function deleteBtnShowHide()
        }
}(jQuery));
    